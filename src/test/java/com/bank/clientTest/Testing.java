package com.bank.clientTest;

import com.bank.client.ClientApplication;
import com.bank.client.controller.ClientController;
import com.bank.client.model.Client;
import com.bank.client.model.FieldValid;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.io.Console;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.CREATED;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class Testing {
    @Autowired
    private WebTestClient testClient;

    public Testing() {
        testClient = WebTestClient.bindToServer().build();
    }

    @Test
    public void test1() {
        testClient.get().uri("http://localhost:8088/client/get/all")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_VALUE)
                .expectBodyList(Client.class);
    }

    @Test
    public void test2() {
        testClient.get().uri("http://localhost:8088/client/getId/{id}", 15)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_VALUE)
                .expectBodyList(Client.class)
                .value(cli -> cli.get(0).getId().intValue(), equalTo(15));
    }

    @Test
    public void test3() {
        //Debe devolver un json con mensaje de error pues el tipo de documento es RUC
        testClient.get().uri("http://localhost:8088/client/getDocument/{typeDocument}/{identityDocument}",
                        "DNI", "22100321453")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_VALUE)
                .expectBodyList(FieldValid.class).value(field->field.get(0).getValue().equals("Cliente con documento 22100321453 no existe"));
        System.out.println("Fin de primer test");
        //DEBE FINALIZAR SIN ERROR
        testClient.get().uri("http://localhost:8088/client/getDocument/{typeDocument}/{identityDocument}",
                        "RUC", "22100321453")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_VALUE)
                .expectBodyList(FieldValid.class);
    }

    @Test
    public void test4() {
        Client newClient=new Client(Long.valueOf(0),"PERSONA","DNI",
                "10090376","CARLOS SOTO","","GENERAL");
        System.out.println("Test: cuando el cliente ya existe. Debe devolver un json con el mensaje Documento 10090376ya existe");
        testClient.post()
                .uri("http://localhost:8088/client/create")
                .body(Mono.justOrEmpty(newClient), Client.class)
                .header( CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .exchange()
                .expectStatus().isOk()
                //.isEqualTo(CREATED)
                .expectBodyList(FieldValid.class);
                //.returnResult(FieldValid.class).getResponseBody()

        Client newClient2=new Client(Long.valueOf(0),"PERSONA","DNI",
                "20090377","CARLOS GUTIERREZ","","GENERAL");
        System.out.println("Test: cuando el cliente NO existe. Debe devolver un json con los datos ceeados");
        testClient.post()
                .uri("http://localhost:8088/client/create")
                .body(Mono.justOrEmpty(newClient2), Client.class)
                .header( CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .exchange()
                .expectStatus().isOk()
                //.isEqualTo(CREATED)
                .expectBodyList(Client.class);


    }
}
