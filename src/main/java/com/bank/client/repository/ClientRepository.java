package com.bank.client.repository;

import com.bank.client.model.Client;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface ClientRepository extends ReactiveMongoRepository<Client, Integer> {
    @Query("{'typeDocument': ?0}{'identityDocument': ?0}")
    //@Query"{'identityDocument': ?0}")

    Mono<Client> findByDocument(String typeDocument, String identityDocument);
}
