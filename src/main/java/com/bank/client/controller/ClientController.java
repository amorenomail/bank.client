package com.bank.client.controller;

import com.bank.client.model.Client;
import com.bank.client.service.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/client")
public class ClientController {
    @Autowired
    private IClientService service;
    //-----------------------------------------------
    @GetMapping(value = "/get/all", produces = { "application/json" })
    @ResponseBody
    public Flux<Client> findAll() {
        return service.findAll();
    }
    //-----------------------------------------------
    @GetMapping(path = { "/getId/{id}" })
    public ResponseEntity<Object> findById(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(service.findById(id).flatMap(c->
        {
            if(c.getTypeClient()==null)
                return Mono.just(service.getErrores());
            else
                return  Mono.just(c);
        }),HttpStatus.OK);
    }
    //-----------------------------------------------
    @GetMapping(value = "/getDocument/{typeDocument}/{identityDocument}", produces = { "application/json" })
    public ResponseEntity<Object> findByDocument(@PathVariable("typeDocument") String typeDocument,
                                       @PathVariable("identityDocument") String identityDocument)
    {
        return new ResponseEntity<>(service.findByDocument(typeDocument,identityDocument).flatMap(c->
        {
            if(c.getTypeClient()==null)
                return Mono.just(service.getErrores());
            else
                return  Mono.just(c);
        }),HttpStatus.OK);
    }
    //-----------------------------------------------
    @PostMapping(path ="/create",produces = { "application/json" })
    public ResponseEntity<Object>  create(@RequestBody Client client) {
        Mono<Client> response=service.create(client);
        if (service.getErrores().size() > 0 ) {
            return new ResponseEntity<>(service.getErrores(), HttpStatus.BAD_REQUEST);
        }
        else {
            return new ResponseEntity<>(response.flatMap(c->{
                if(c.getTypeClient()==null)
                    return Mono.just(service.getErrores());
                else
                    return  Mono.just(c);
            }), HttpStatus.OK);
        }
    }
    //-----------------------------------------------
    @PutMapping(path = { "/update/{id}" }, produces = { "application/json" })
    public ResponseEntity<Object> update(@PathVariable("id") Integer id,
            @RequestBody Client client) throws Exception {
        Mono<Client> response=service.update(id, client);
        if (service.getErrores().size() > 0 ) {
            return new ResponseEntity<>(service.getErrores(), HttpStatus.BAD_REQUEST);
        }
        else {
            return new ResponseEntity<>(response.flatMap(c->{
                if(c.getTypeClient()==null) {
                    return Mono.just(service.getErrores());
                }
                else
                    return  Mono.just(c);
            }), HttpStatus.OK);
        }
    }
    //-----------------------------------------------
    @DeleteMapping(path = { "delete/{id}" })
    public void deleteById(@PathVariable("id") Integer id) throws Exception {
        service.delete(id);
    }

}
