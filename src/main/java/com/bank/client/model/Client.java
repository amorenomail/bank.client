package com.bank.client.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.beans.JavaBean;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "client")
public class Client {
    @Transient
    public static final String SEQUENCE_NAME = "client_sequence";
    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String typeClient;
    private String typeDocument;
    private String identityDocument;
    private String names;
    private String company;
    private String profile;//proy2
}
