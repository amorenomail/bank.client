package com.bank.client.service;

import com.bank.client.model.FieldValid;
import com.bank.client.repository.ClientRepository;
import com.bank.client.model.Client;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService implements IClientService{
    @Autowired
    ClientRepository repository;

    private List<FieldValid> fieldsValid;

    private boolean validInfo(Client client)
    {
        String strValidTypeClient="PERSONA|EMPRESA";
        String strValidTypeDocument="DNI|RUC";
        String strValidProfilePersona="VIP|GENERAL";
        String strValidProfileEmpresa="PYME|GENERAL";

        fieldsValid=new ArrayList<>();

        if(!strValidTypeClient.contains(client.getTypeClient()))
        {
            fieldsValid.add(new FieldValid("typeClient","typeCient debe tener el valor de " + strValidTypeClient));
        }
          if(!strValidTypeDocument.contains(client.getTypeDocument()))
          {
              fieldsValid.add(new FieldValid("typeDocument","typeDocument debe tener el valor de " + strValidTypeDocument));
          }
          //proy2
          if(client.getTypeClient().equals("PERSONA"))
          {
              if(!strValidProfilePersona.contains(client.getProfile()))
              {
                  fieldsValid.add(new FieldValid("profile","profile debe tener el valor de " + strValidProfilePersona));
              }
          }
          if(client.getTypeClient().equals("EMPRESA"))
            {
                if(!strValidProfileEmpresa.contains(client.getProfile()))
                {
                    fieldsValid.add(new FieldValid("profile","profile debe tener el valor de " + strValidProfileEmpresa));
                }
            }
          //fin proy2
          if(client.getIdentityDocument()==null || client.getIdentityDocument().equals(""))
          {
              fieldsValid.add(new FieldValid("identityDocument","identityDocument no puede ser nulo"));
          }
          if(client.getTypeClient().equals("PERSONA") && (client.getNames()==null || client.getNames().equals("")))
          {
              fieldsValid.add(new FieldValid("names","names no puede ser nulo si es PERSONA"));
          }
          if (client.getTypeClient().equals("EMPRESA") && client.getCompany()==null )
          {
              fieldsValid.add(new FieldValid("company","company no puede ser nulo si es EMPRESA"));;
          }
         if(fieldsValid.size()>0)
             return false;
         else
             return true;
        //
    };

    public List<FieldValid> getErrores()
    {
        return fieldsValid;
    }

    @Override
    public Mono<Client> create(Client client) {
        String document=client.getIdentityDocument();
        String typeDocument =client.getTypeDocument();
        String typeClient =client.getTypeClient();

        if(this.validInfo(client)==false)
        {
            return Mono.empty();
        }

        client.setId(Long.valueOf(0));

        return repository.findAll().filter(v->(v.getTypeClient().equals(typeClient)
                                                && v.getIdentityDocument().equals(document)))
                .map(vf->{
                    return new Client();
                })
                .defaultIfEmpty(client).last()
               .flatMap(cli->{
                    if(cli.getIdentityDocument()==null) {
                        fieldsValid.add(new FieldValid("error","Documento "+ document + "ya existe"));
                        return  Mono.just(cli);
                    }
                    else
                        return this.repository.save(cli);
                });
    }

    @Override
    public Mono<Client> findById(Integer id) {
        return repository.findById(id)
                .defaultIfEmpty(new Client())
                .flatMap(c->{
                    fieldsValid=new ArrayList<>();
                    fieldsValid.add(new FieldValid("error","Cliente con "+ String.valueOf(id) + " no existe"));
                    return Mono.just(c);
                });
    }

    @Override
    public Flux<Client> findAll() {
        return repository.findAll();
    }

    @Override
    public Mono<Client> findByDocument(String typeDocument, String identityDocument)
    {
        return repository.findAll().filter(x ->
                x.getTypeDocument().equals(typeDocument) && x.getIdentityDocument().equals(identityDocument))
                .defaultIfEmpty(new Client())
                .flatMap(c->{
                    if(c.getId()==null) {
                        fieldsValid = new ArrayList<>();
                        fieldsValid.add(new FieldValid("error", "Cliente con documento " + identityDocument + " no existe"));
                    }
                    return Mono.just(c);
                }) .last();
    }

    @Override
    public Mono<Client> update(Integer id,Client client) {
        String document=client.getIdentityDocument();
        String typeDocument =client.getTypeDocument();
        String typeClient =client.getTypeClient();
        //fieldsValid = new ArrayList<>();

        if(this.validInfo(client)==false)
        {
            return Mono.empty();
        }
        fieldsValid.clear();

        return repository.findById(id)
                .defaultIfEmpty(new Client())
                .flatMap(p -> {
                    if(p.getId()==null) {
                        //fieldsValid = new ArrayList<>();
                        fieldsValid.add(new FieldValid("error", "Cliente conXX ID " + String.valueOf(id) + " no existe"));
                        return Mono.just(p);
                    }
                    else
                    {
                        return repository.findAll().filter(cc->cc.getTypeDocument().equals(typeDocument)
                                                && cc.getIdentityDocument().equals(document) && (cc.getId().intValue() != id))
                                        .flatMap(cf->{
                                            return Mono.just(new Client());})
                                         .defaultIfEmpty(p).last();
                    }
                })
                .flatMap(cs->{
                    System.out.println(cs.getTypeClient());
                    if(cs.getTypeClient()==null)
                    {
                        if(fieldsValid.size()==0) {
                            fieldsValid = new ArrayList<>();
                            fieldsValid.add(new FieldValid("error", "Tipo de documento y número de documento ya existen"));
                        }
                    }
                    else
                    {
                        System.out.println("ZZZZCCCCUUUUUUUAAAA");
                        cs.setId(client.getId());
                        cs.setTypeClient(client.getTypeClient());
                        cs.setTypeDocument(client.getTypeDocument());
                        cs.setIdentityDocument(client.getIdentityDocument());
                        cs.setNames(client.getNames());
                        cs.setCompany(client.getCompany());
                    };
                    return Mono.just(cs);
                }).flatMap(cf->{
                    if(cf.getTypeClient()!=null)
                        return this.repository.save(cf);
                    return Mono.just(cf);
                });
    }

    @Override
    public Mono<Void> delete(Integer id) {
        return repository.deleteById(id);
    }

}
