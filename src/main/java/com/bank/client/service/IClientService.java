package com.bank.client.service;

import com.bank.client.model.Client;
import com.bank.client.model.FieldValid;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface IClientService {
    Mono<Client> create(Client e);

    Mono<Client> findById(Integer id);

    Flux<Client> findAll();

    Mono<Client> findByDocument(String typeDocument, String identityDocument);

    Mono<Client> update(Integer id,Client client);

    Mono<Void> delete(Integer id);

    public List<FieldValid> getErrores();
}
